package com.radroid.sampleapp.presentation.view;

public interface AddNoteView {

    void showAddNoteFailureMessage();

    void showAddNoteSuccessfulMessage();

    void finishWithSuccess();

    void finishWithFailure();

    void showAddNoteDiscardedMessage();

    void showEmptyNoteTitleError();

    void showLogoutMessage();

    void showLoginScreen();
}
