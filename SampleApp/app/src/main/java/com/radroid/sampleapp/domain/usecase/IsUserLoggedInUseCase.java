package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.AuthenticationGateway;

import io.reactivex.Single;

class IsUserLoggedInUseCase implements SingleUseCase<Boolean> {

    private final AuthenticationGateway authenticationGateway;

    public IsUserLoggedInUseCase(AuthenticationGateway authenticationGateway) {
        this.authenticationGateway = authenticationGateway;
    }

    @Override
    public Single<Boolean> execute() {
        return authenticationGateway.isUserLoggedIn();
    }
}
