package com.radroid.sampleapp.presentation.view;

public interface LauncherView {

    void showNoteListScreen();

    void showLoginScreen();
}
