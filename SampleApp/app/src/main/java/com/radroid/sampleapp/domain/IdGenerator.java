package com.radroid.sampleapp.domain;

import java.util.UUID;

//enum singleton
public enum IdGenerator {
    INSTANCE;

    public String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
