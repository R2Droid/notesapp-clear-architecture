package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.AuthenticationGateway;

import io.reactivex.Completable;

class LogoutUseCase implements CompletableUseCase {

    private AuthenticationGateway authenticationGateway;

    public LogoutUseCase(AuthenticationGateway authenticationGateway) {
        this.authenticationGateway = authenticationGateway;
    }

    @Override
    public Completable execute() {
        return authenticationGateway.logout();
    }
}
