package com.radroid.sampleapp.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.radroid.sampleapp.R;
import com.radroid.sampleapp.android.connector.LoginViewConnector;
import com.radroid.sampleapp.android.navigator.LoginViewNavigator;
import com.radroid.sampleapp.presentation.presenter.LoginViewPresenter;
import com.radroid.sampleapp.presentation.view.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.login_activity_toolbar)
    Toolbar toolbar;

    @BindView(R.id.login_activity_email_et)
    EditText emailEditText;

    @BindView(R.id.login_activity_password_et)
    EditText passwordEditText;

    private LoginViewPresenter presenter;
    private LoginViewNavigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginViewConnector connector = new LoginViewConnector(this);
        navigator = new LoginViewNavigator(this);

        ButterKnife.bind(this);
        initToolbar();
        presenter.onViewCreated(this);
    }

    public static Intent newIntent(Activity activity) {
        return new Intent(activity, LoginActivity.class);
    }

    public static Intent newIntentWithNewClearTask(Activity activity) {
        Intent newClearTaskIntent = newIntent(activity);

        newClearTaskIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        return newClearTaskIntent;
    }

    public void setPresenter(LoginViewPresenter presenter) {
        this.presenter = presenter;
    }

    @OnClick(R.id.login_activity_log_btn)
    public void onLoginClicked() {
        presenter.onLoginClicked(emailEditText.getText().toString(), passwordEditText.getText().toString());
    }

    @Override
    public void showNoteListScreen() {
        navigator.showNoteListScreen();
        finish();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }
}
