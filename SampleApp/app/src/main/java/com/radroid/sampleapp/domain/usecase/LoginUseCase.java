package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.AuthenticationGateway;

import io.reactivex.Completable;

class LoginUseCase implements CompletableUseCase {

    private final AuthenticationGateway authenticationGateway;
    private final String username;
    private final String password;

    public LoginUseCase(AuthenticationGateway authenticationGateway, String username, String password) {
        this.authenticationGateway = authenticationGateway;
        this.username = username;
        this.password = password;
    }

    @Override
    public Completable execute() {
        return authenticationGateway.login(username.trim(), password.trim());
    }
}
