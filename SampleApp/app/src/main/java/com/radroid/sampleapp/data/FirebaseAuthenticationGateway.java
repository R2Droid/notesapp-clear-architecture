package com.radroid.sampleapp.data;

import com.google.firebase.auth.FirebaseAuth;

import io.reactivex.Completable;
import io.reactivex.Single;

public class FirebaseAuthenticationGateway implements AuthenticationGateway {

    private final FirebaseAuth firebaseAuth;

    public FirebaseAuthenticationGateway(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public Completable login(String email, String password) {
        return Completable.create(emitter -> firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(task.getException());
                    }
                }));
    }

    @Override
    public Single<Boolean> isUserLoggedIn() {
        return Single.fromCallable(() -> firebaseAuth.getCurrentUser() != null);
    }

    @Override
    public Completable logout() {
        return Completable.fromAction(() -> firebaseAuth.signOut());
    }

}
