package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.EntityGateway;

import io.reactivex.Completable;

class DeleteNoteUseCase implements CompletableUseCase {

    private final EntityGateway entityGateway;
    private final String noteId;

    public DeleteNoteUseCase(EntityGateway entityGateway, String noteId) {
        this.entityGateway = entityGateway;
        this.noteId = noteId;
    }

    @Override
    public Completable execute() {
        return entityGateway.deleteNote(noteId);
    }
}
