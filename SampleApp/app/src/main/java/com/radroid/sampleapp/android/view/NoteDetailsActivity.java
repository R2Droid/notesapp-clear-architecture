package com.radroid.sampleapp.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.radroid.sampleapp.R;
import com.radroid.sampleapp.android.connector.NoteDetailsViewConnector;
import com.radroid.sampleapp.android.navigator.NoteDetailsNavigator;
import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.presentation.presenter.NoteDetailsPresenter;
import com.radroid.sampleapp.presentation.view.NoteDetailsView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteDetailsActivity extends AppCompatActivity implements NoteDetailsView {

    public static final String NOTE_ID_EXTRA = "NOTE_ID_EXTRA";

    @BindView(R.id.note_activity_toolbar)
    Toolbar toolbar;

    @BindView(R.id.note_activity_title_edit_text)
    EditText titleEditText;

    @BindView(R.id.note_activity_content_edit_text)
    EditText contentEditText;

    @BindString(R.string.note_empty_title_error_message)
    String emptyTitleErrorMessage;

    private NoteDetailsPresenter presenter;
    private NoteDetailsNavigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        NoteDetailsViewConnector connector = new NoteDetailsViewConnector(this);
        navigator = new NoteDetailsNavigator(this);

        ButterKnife.bind(this);
        initViews();
        presenter.onViewCreated(this);
    }

    public static Intent newIntent(Activity noteListActivity, String noteId) {
        Intent intent = new Intent(noteListActivity, NoteDetailsActivity.class);

        intent.putExtra(NOTE_ID_EXTRA, noteId);

        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_details_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.details_note_menu_edit:
                presenter.onSaveNoteClicked();
                break;
            case R.id.details_note_menu_delete:
                presenter.onDeleteNoteClicked();
                break;
            case R.id.details_note_menu_logout:
                presenter.onLogoutClicked();
                break;
            default:
                Logger.e("Item id: " + item.getItemId() + " is not handled");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        presenter.onSaveNoteClicked();
        super.onBackPressed();
    }

    public void setPresenter(NoteDetailsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showNoteInitializationError() {
        Toast.makeText(this, R.string.note_initialization_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setNoteTitleView(String title) {
        titleEditText.setText(title);
        titleEditText.setSelection(title.length());
    }

    @Override
    public void setNoteContentView(String content) {
        contentEditText.setText(content);

        if (content != null) {
            contentEditText.setSelection(content.length());
        }
    }

    @Override
    public void finishWithSuccess() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void showEditNoteErrorMessage() {
        Toast.makeText(this, R.string.note_edit_error_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishWithFailure() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    public void showEditNoteSuccessMessage() {
        Toast.makeText(this, R.string.note_edit_success_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyNoteTitleErrorMessage() {
        titleEditText.setError(emptyTitleErrorMessage);
    }

    @Override
    public void showEditNoteDeleteSuccessMessage() {
        Toast.makeText(this, R.string.note_delete_success_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEditNoteDeleteErrorMessage() {
        Toast.makeText(this, R.string.note_delete_error_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLogoutMessage() {
        Toast.makeText(this, R.string.logout_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoginScreen() {
        navigator.showLoginScreen();
        finish();
    }

    @Override
    public void showNoteEditChangesDiscardedMessage() {
        Toast.makeText(this, R.string.note_edit_changes_discarded_message, Toast.LENGTH_SHORT).show();
    }

    private void initViews() {
        initToolbar();
        initEditText();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initEditText() {
        titleEditText.addTextChangedListener(new SimpleTextWatcher() {

            @Override
            public void afterTextChanged(Editable title) {
                presenter.onNoteTitleChanged(title.toString());
            }
        });

        contentEditText.addTextChangedListener(new SimpleTextWatcher() {

            @Override
            public void afterTextChanged(Editable content) {
                presenter.onNoteContentChanged(content.toString());
            }
        });
    }
}
