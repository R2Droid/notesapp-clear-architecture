package com.radroid.sampleapp.android.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.radroid.sampleapp.R;
import com.radroid.sampleapp.presentation.view.NoteItemView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteViewHolder extends RecyclerView.ViewHolder implements NoteItemView {

    @BindView(R.id.item_note_title_tv)
    TextView titleTextView;

    @BindView(R.id.item_note_content_tv)
    TextView contentTextView;

    public NoteViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void displayNoteTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void displayNoteContent(String content) {
        contentTextView.setText(content);
    }
}
