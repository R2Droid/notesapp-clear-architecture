package com.radroid.sampleapp.domain;

public interface CustomLogger {

    void d(String message, Object... params);

    void e(Throwable throwable, String message);
}
