package com.radroid.sampleapp.android.connector;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.radroid.sampleapp.android.view.NoteListActivity;
import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.data.FirebaseAuthenticationGateway;
import com.radroid.sampleapp.data.FirebaseCloudEntityGateway;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.presenter.NoteListPresenter;

public class NoteListViewConnector {

    public NoteListViewConnector(NoteListActivity noteListActivity) {
        EntityGateway entityGateway = new FirebaseCloudEntityGateway(FirebaseDatabase.getInstance());
        FirebaseAuthenticationGateway authGateway = new FirebaseAuthenticationGateway(FirebaseAuth.getInstance());
        UseCaseFactory usecaseFactory = new UseCaseFactory(entityGateway, authGateway);
        NoteListPresenter presenter = new NoteListPresenter(usecaseFactory);

        noteListActivity.setPresenter(presenter);
    }
}
