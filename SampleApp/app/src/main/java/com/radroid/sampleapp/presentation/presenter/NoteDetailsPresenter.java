package com.radroid.sampleapp.presentation.presenter;

import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.NoteViewModel;
import com.radroid.sampleapp.presentation.view.NoteDetailsView;

public class NoteDetailsPresenter {

    private UseCaseFactory useCaseFactory;
    private String noteId;
    private NoteDetailsView view;
    private NoteViewModel noteViewModel;

    public NoteDetailsPresenter(UseCaseFactory useCaseFactory, String noteId) {
        this.useCaseFactory = useCaseFactory;
        this.noteId = noteId;
    }

    public void onViewCreated(NoteDetailsView view) {
        this.view = view;
        initializeNoteView();
    }

    public void onNoteTitleChanged(String title) {
        noteViewModel.setTitle(title);
    }

    public void onNoteContentChanged(String content) {
        noteViewModel.setContent(content);
    }

    public void onSaveNoteClicked() {
        String noteTitle = noteViewModel.getTitle();

        if (noteTitle == null || noteTitle.isEmpty()) {
            view.showEmptyNoteTitleErrorMessage();
        } else {
            useCaseFactory.editNote(noteViewModel)
                    .execute()
                    .subscribe(
                            () -> {
                                view.showEditNoteSuccessMessage();
                                view.finishWithSuccess();
                            },
                            error -> {
                                Logger.e(error);
                                view.showEditNoteErrorMessage();
                                view.finishWithFailure();
                            }
                    );
        }
    }

    public void onDeleteNoteClicked() {

        useCaseFactory.deleteNote(noteId)
                .execute()
                .subscribe(() -> {
                            view.showEditNoteDeleteSuccessMessage();
                            view.finishWithSuccess();
                        },
                        error -> {
                            Logger.e(error);
                            view.showEditNoteDeleteErrorMessage();
                            view.finishWithFailure();
                        }

                );
    }

    public void onLogoutClicked() {
        useCaseFactory.logout()
                .execute()
                .subscribe(() -> {
                            view.showNoteEditChangesDiscardedMessage();
                            view.showLogoutMessage();
                            view.showLoginScreen();

                        },
                        error -> Logger.e(error.getMessage()));
    }

    private void initializeNoteView() {

        useCaseFactory.initializeNoteViews(noteId)
                .execute()
                .subscribe(
                        note -> {
                            noteViewModel = new NoteViewModel(note);
                            view.setNoteTitleView(note.getTitle());
                            view.setNoteContentView(note.getContent());
                        },
                        error -> {
                            Logger.e(error);
                            view.showNoteInitializationError();
                        }
                );
    }
}
