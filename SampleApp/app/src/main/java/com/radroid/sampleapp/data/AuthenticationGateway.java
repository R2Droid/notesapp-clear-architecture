package com.radroid.sampleapp.data;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthenticationGateway {

    Completable login(String username, String password);

    Single<Boolean> isUserLoggedIn();

    Completable logout();
}
