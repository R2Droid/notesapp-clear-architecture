package com.radroid.sampleapp.android;

import android.util.Log;

import com.radroid.sampleapp.domain.CustomLogger;

class AndroidLogger implements CustomLogger {

    private static final String TAG = AndroidLogger.class.getSimpleName();

    @Override
    public void d(String message, Object... params) {
        Log.d(TAG, String.format(message, params));
    }

    @Override
    public void e(Throwable throwable, String message) {

        if (throwable == null) {
            Log.e(TAG, message);
        } else {
            Log.e(TAG, message == null ? "" : message, throwable);
        }
    }
}
