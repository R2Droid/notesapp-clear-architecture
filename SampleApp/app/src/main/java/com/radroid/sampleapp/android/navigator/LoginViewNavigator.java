package com.radroid.sampleapp.android.navigator;

import com.radroid.sampleapp.android.view.LoginActivity;
import com.radroid.sampleapp.android.view.NoteListActivity;

public class LoginViewNavigator {

    private LoginActivity activity;

    public LoginViewNavigator(LoginActivity activity) {
        this.activity = activity;
    }

    public void showNoteListScreen() {
        activity.startActivity(NoteListActivity.newIntent(activity));
    }
}
