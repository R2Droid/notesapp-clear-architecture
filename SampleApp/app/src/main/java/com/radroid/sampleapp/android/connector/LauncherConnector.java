package com.radroid.sampleapp.android.connector;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.radroid.sampleapp.android.view.LauncherActivity;
import com.radroid.sampleapp.data.AuthenticationGateway;
import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.data.FirebaseAuthenticationGateway;
import com.radroid.sampleapp.data.FirebaseCloudEntityGateway;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.presenter.LauncherPresenter;

public class LauncherConnector {

    public LauncherConnector(LauncherActivity launcherActivity) {
        AuthenticationGateway authenticationGateway = new FirebaseAuthenticationGateway(FirebaseAuth.getInstance());
        EntityGateway entityGateway = new FirebaseCloudEntityGateway(FirebaseDatabase.getInstance());
        UseCaseFactory usecaseFactory = new UseCaseFactory(entityGateway, authenticationGateway);
        LauncherPresenter presenter = new LauncherPresenter(usecaseFactory);

        launcherActivity.setPresenter(presenter);
    }
}
