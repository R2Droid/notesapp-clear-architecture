package com.radroid.sampleapp.data;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.radroid.sampleapp.domain.entity.Note;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;

public class FirebaseCloudEntityGateway implements EntityGateway {

    private static final String USERS_PATH = "users";
    private static final String NOTES_LIST_PATH = "note_list";
    private static final String TIMESTAMP = "timestamp";
    private final FirebaseDatabase firebaseDatabase;

    public FirebaseCloudEntityGateway(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    @Override
    public Single<List<Note>> fetchNotesList() {
        return Single.create(emitter -> {

            ValueEventListener listener = new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot notesSnapshot) {
                    List<Note> noteList = getNotesListFromSnapshot(notesSnapshot);

                    emitter.onSuccess(noteList);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            };

            getNoteListDatabaseReference()
                    .orderByChild(TIMESTAMP)
                    .addListenerForSingleValueEvent(listener);

        });
    }

    @Override
    public Completable addNote(Note note) {
        return Completable.create(emitter -> {
            FirebaseNoteEntity firebaseNoteEntity = new FirebaseNoteEntity(note);
            String noteId = note.getId();
            getNoteByIdDatabaseReference(noteId)
                    .setValue(firebaseNoteEntity, (databaseError, databaseReference) -> {
                        if (databaseError == null) {
                            emitter.onComplete();
                        } else {
                            emitter.onError(databaseError.toException());
                        }
                    });

            getNoteByIdDatabaseReference(noteId)
                    .child(TIMESTAMP)
                    .setValue(ServerValue.TIMESTAMP)
                    .isSuccessful();
        });
    }

    @Override
    public Completable updateNote(Note note) {
        return Completable.create(emitter -> {
            Map<String, Object> updateMap = new HashMap<>();
            FirebaseNoteEntity firebaseNote = new FirebaseNoteEntity(note);
            String noteId = note.getId();

            updateMap.put(noteId, firebaseNote);

            getNoteListDatabaseReference()
                    .updateChildren(updateMap, (databaseError, databaseReference) -> {
                        if (databaseError == null) {
                            emitter.onComplete();
                        } else {
                            emitter.onError(databaseError.toException());
                        }
                    });
        });
    }

    @Override
    public Single<Note> getNoteById(String noteId) {
        return Single.create(emitter -> {

            ValueEventListener listener = new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot notesSnapshot) {
                    FirebaseNoteEntity firebaseNote = notesSnapshot.getValue(FirebaseNoteEntity.class);
                    Note note = firebaseNote.toNote();

                    emitter.onSuccess(note);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            };

            getNoteByIdDatabaseReference(noteId)
                    .addListenerForSingleValueEvent(listener);
        });

    }

    @Override
    public Completable deleteNote(String noteId) {
        return Completable.create(emitter -> getNoteListDatabaseReference()
                .child(noteId)
                .removeValue((databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(databaseError.toException());
                    }
                })
        );
    }

    private DatabaseReference getNoteByIdDatabaseReference(String noteId) {
        return getNoteListDatabaseReference()
                .child(noteId);
    }

    private DatabaseReference getNoteListDatabaseReference() {
        return firebaseDatabase
                .getReference()
                .child(USERS_PATH)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(NOTES_LIST_PATH);
    }

    private List<Note> getNotesListFromSnapshot(DataSnapshot notesSnapshot) {
        List<Note> notesList = new ArrayList<>();

        for (DataSnapshot noteSnapshot : notesSnapshot.getChildren()) {
            FirebaseNoteEntity firebaseNote = noteSnapshot.getValue(FirebaseNoteEntity.class);
            Note note = firebaseNote.toNote();

            notesList.add(note);
        }

        Collections.reverse(notesList);
        return notesList;
    }
}
