package com.radroid.sampleapp.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.radroid.sampleapp.R;
import com.radroid.sampleapp.android.connector.NoteListViewConnector;
import com.radroid.sampleapp.android.navigator.NoteListViewNavigator;
import com.radroid.sampleapp.android.view.adapter.NoteViewHolder;
import com.radroid.sampleapp.android.view.adapter.NotesRecyclerViewAdapter;
import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.domain.entity.Note;
import com.radroid.sampleapp.presentation.presenter.NoteListPresenter;
import com.radroid.sampleapp.presentation.view.NoteListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoteListActivity extends AppCompatActivity implements NoteListView {

    private static final int NOTE_DETAILS_REQUEST_CODE = 1000;
    private static final int NOTE_ADD_REQUEST_CODE = 1001;

    @BindView(R.id.note_list_activity_toolbar)
    Toolbar toolbar;

    @BindView(R.id.note_list_activity_notes_rv)
    RecyclerView notesRecyclerView;

    private NoteListPresenter presenter;
    private NoteListViewNavigator navigator;
    private RecyclerView.Adapter<NoteViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        NoteListViewConnector connector = new NoteListViewConnector(this);
        navigator = new NoteListViewNavigator(this);

        ButterKnife.bind(this);
        initNoteList();
        initToolbar();
        presenter.onViewCreated(this);
    }

    public static Intent newIntent(Activity launcherActivity) {
        return new Intent(launcherActivity, NoteListActivity.class);
    }

    public void setPresenter(NoteListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void updateNotesList(List<Note> noteList) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showNoteDetailsScreen(String noteId) {
        navigator.showNoteDetailsScreen(NOTE_DETAILS_REQUEST_CODE, noteId);
    }

    @Override
    public void showLogoutMessage() {
        Toast.makeText(this, R.string.logout_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoginScreen() {
        navigator.showLoginScreen();
    }

    @OnClick(R.id.note_list_add_note_fab)
    public void onAddNoteClicked() {
        navigator.showAddNoteScreen(NOTE_ADD_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.menu_note_list, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.note_list_menu_logout:
                presenter.onLogoutClicked();
                break;
            default:
                Logger.e("Item id: " + item.getItemId() + " is not handled");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NOTE_DETAILS_REQUEST_CODE:
            case NOTE_ADD_REQUEST_CODE:
                handleNoteOperations(resultCode);
                break;
            default:
                Log.d(NoteListActivity.class.getSimpleName(), "OnActivityResult request code is not handled: " + requestCode);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleNoteOperations(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            presenter.onNoteListDataChanged();
        }
    }

    private void initNoteList() {
        adapter = new NotesRecyclerViewAdapter(this, presenter);
        notesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        notesRecyclerView.setAdapter(adapter);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }
}
