package com.radroid.sampleapp.domain;

public final class Logger {

    private static CustomLogger logger = new NoOpLogger();

    public static void setLogger(CustomLogger logger) {
        Logger.logger = logger;
    }

    public static void d(String message, Object... params) {
        logger.d(message, params);
    }

    public static void e(String message) {
        e(null, message);
    }

    public static void e(Throwable throwable) {
        e(throwable, throwable.getMessage());
    }

    public static void e(Throwable throwable, String message) {
        logger.e(throwable, message);
    }

    private Logger() {
        throw new UnsupportedOperationException("No instances");
    }
}
