package com.radroid.sampleapp.data;

import com.radroid.sampleapp.domain.entity.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface EntityGateway {

    Single<List<Note>> fetchNotesList();

    Completable addNote(Note note);

    Completable updateNote(Note note);

    Single<Note> getNoteById(String noteId);

    Completable deleteNote(String noteId);
}
