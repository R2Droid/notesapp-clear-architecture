package com.radroid.sampleapp.android.navigator;

import com.radroid.sampleapp.android.view.AddNoteActivity;
import com.radroid.sampleapp.android.view.LoginActivity;

public class AddNoteNavigator {

    private AddNoteActivity activity;

    public AddNoteNavigator(AddNoteActivity activity) {
        this.activity = activity;
    }

    public void showLoginScreen() {
        activity.startActivity(LoginActivity.newIntentWithNewClearTask(activity));
    }
}
