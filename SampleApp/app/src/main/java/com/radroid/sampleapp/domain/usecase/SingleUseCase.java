package com.radroid.sampleapp.domain.usecase;

import io.reactivex.Single;

public interface SingleUseCase<T> {

    Single<T> execute();
}
