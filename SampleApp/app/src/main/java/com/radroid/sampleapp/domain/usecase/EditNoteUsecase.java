package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.domain.entity.Note;
import com.radroid.sampleapp.presentation.NoteViewModel;

import io.reactivex.Completable;

class EditNoteUsecase implements CompletableUseCase {

    private final EntityGateway entityGateway;
    private final NoteViewModel noteViewModel;

    public EditNoteUsecase(EntityGateway entityGateway, NoteViewModel noteViewModel) {
        this.entityGateway = entityGateway;
        this.noteViewModel = noteViewModel;
    }

    @Override
    public Completable execute() {
        Note note = noteViewModel.createNoteFromViewModel();

        return entityGateway.updateNote(note);
    }
}
