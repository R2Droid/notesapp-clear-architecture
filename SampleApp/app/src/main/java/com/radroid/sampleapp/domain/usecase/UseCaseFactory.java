package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.AuthenticationGateway;
import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.domain.entity.Note;
import com.radroid.sampleapp.presentation.NoteViewModel;

import java.util.List;

public class UseCaseFactory {

    private final EntityGateway entityGateway;
    private AuthenticationGateway authenticationGateway;

    public UseCaseFactory(EntityGateway entityGateway, AuthenticationGateway authenticationGateway) {
        this.entityGateway = entityGateway;
        this.authenticationGateway = authenticationGateway;
    }

    public SingleUseCase<List<Note>> showNotesList() {
        return new ShowNotesListUseCase(entityGateway);
    }

    public SingleUseCase<Boolean> isUserLoggedIn() {
        return new IsUserLoggedInUseCase(authenticationGateway);
    }

    public CompletableUseCase login(String username, String password) {
        return new LoginUseCase(authenticationGateway, username.trim(), password.trim());
    }

    public CompletableUseCase addNote(NoteViewModel noteViewModel) {
        return new AddNoteUsecase(entityGateway, noteViewModel);
    }

    public SingleUseCase<Note> initializeNoteViews(String noteId) {
        return new InitializeNoteViewsUseCase(entityGateway, noteId);
    }

    public CompletableUseCase editNote(NoteViewModel noteViewModel) {
        return new EditNoteUsecase(entityGateway, noteViewModel);
    }

    public CompletableUseCase deleteNote(String noteId) {
        return new DeleteNoteUseCase(entityGateway, noteId);
    }

    public CompletableUseCase logout() {
        return new LogoutUseCase(authenticationGateway);
    }
}
