package com.radroid.sampleapp.android.navigator;

import com.radroid.sampleapp.android.view.AddNoteActivity;
import com.radroid.sampleapp.android.view.LoginActivity;
import com.radroid.sampleapp.android.view.NoteDetailsActivity;
import com.radroid.sampleapp.android.view.NoteListActivity;

public class NoteListViewNavigator {

    private final NoteListActivity activity;

    public NoteListViewNavigator(NoteListActivity activity) {
        this.activity = activity;
    }

    public void showNoteDetailsScreen(int requestCode, String noteId) {
        activity.startActivityForResult(NoteDetailsActivity.newIntent(activity, noteId), requestCode);
    }

    public void showAddNoteScreen(int requestCode) {
        activity.startActivityForResult(AddNoteActivity.newIntent(activity), requestCode);
    }

    public void showLoginScreen() {
        activity.startActivity(LoginActivity.newIntentWithNewClearTask(activity));
    }
}
