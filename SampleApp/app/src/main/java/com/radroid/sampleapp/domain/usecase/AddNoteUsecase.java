package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.domain.IdGenerator;
import com.radroid.sampleapp.domain.entity.Note;
import com.radroid.sampleapp.presentation.NoteViewModel;

import io.reactivex.Completable;

class AddNoteUsecase implements CompletableUseCase {

    private EntityGateway entityGateway;
    private Note note;

    public AddNoteUsecase(EntityGateway entityGateway, NoteViewModel note) {
        note.setId(IdGenerator.INSTANCE.generateUUID());
        this.entityGateway = entityGateway;
        this.note = note.createNoteFromViewModel();
    }

    @Override
    public Completable execute() {
        return entityGateway.addNote(note);
    }
}
