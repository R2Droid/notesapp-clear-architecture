package com.radroid.sampleapp.presentation.presenter;

import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.view.LauncherView;

import io.reactivex.disposables.Disposable;

public class LauncherPresenter {

    private final UseCaseFactory usecaseFactory;
    private LauncherView view;
    private Disposable disposable;

    public LauncherPresenter(UseCaseFactory usecaseFactory) {
        this.usecaseFactory = usecaseFactory;
    }

    public void onViewCreated(LauncherView view) {
        this.view = view;

        disposable = usecaseFactory.isUserLoggedIn()
                .execute()
                .subscribe(
                        isLoggedIn -> {
                            if (isLoggedIn) {
                                view.showNoteListScreen();
                            } else {
                                view.showLoginScreen();
                            }
                        },
                        error -> Logger.e(error)
                );
    }

    public void onViewDestroyed() {
        disposable.dispose();
    }
}
