package com.radroid.sampleapp.presentation;

import com.radroid.sampleapp.domain.entity.Note;

public class NoteViewModel {

    private String id;
    private String title;
    private String content;
    private long timestamp;

    public NoteViewModel(Note note) {
        this.id = note.getId();
        this.title = note.getTitle();
        this.content = note.getContent();
    }

    public NoteViewModel() {
    }

    public void setTitle(String title) {
        this.title = title.trim();
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Note createNoteFromViewModel() {
        return new Note(
                this.id,
                this.title,
                this.content,
                this.timestamp
        );
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
}
