package com.radroid.sampleapp.android.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.radroid.sampleapp.android.connector.LauncherConnector;
import com.radroid.sampleapp.android.navigator.LauncherNavigator;
import com.radroid.sampleapp.presentation.presenter.LauncherPresenter;
import com.radroid.sampleapp.presentation.view.LauncherView;

public class LauncherActivity extends AppCompatActivity implements LauncherView {

    private LauncherPresenter presenter;
    private LauncherNavigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LauncherConnector connector = new LauncherConnector(this);
        navigator = new LauncherNavigator(this);

        presenter.onViewCreated(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    public void setPresenter(LauncherPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showNoteListScreen() {
        navigator.showNotesListScreen();
        finish();
    }

    @Override
    public void showLoginScreen() {
        navigator.showLoginScreen();
        finish();
    }
}
