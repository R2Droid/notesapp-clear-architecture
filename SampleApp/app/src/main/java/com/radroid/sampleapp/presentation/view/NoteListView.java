package com.radroid.sampleapp.presentation.view;

import com.radroid.sampleapp.domain.entity.Note;

import java.util.List;

public interface NoteListView {

    void updateNotesList(List<Note> noteList);

    void showNoteDetailsScreen(String noteId);

    void showLogoutMessage();

    void showLoginScreen();
}
