package com.radroid.sampleapp.android.connector;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.radroid.sampleapp.android.view.AddNoteActivity;
import com.radroid.sampleapp.data.AuthenticationGateway;
import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.data.FirebaseAuthenticationGateway;
import com.radroid.sampleapp.data.FirebaseCloudEntityGateway;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.presenter.AddNotePresenter;

public class AddNoteViewConnector {

    public AddNoteViewConnector(AddNoteActivity addNoteActivity) {
        EntityGateway entityGateway = new FirebaseCloudEntityGateway(FirebaseDatabase.getInstance());
        AuthenticationGateway authGateway = new FirebaseAuthenticationGateway(FirebaseAuth.getInstance());
        UseCaseFactory useCaseFactory = new UseCaseFactory(entityGateway, authGateway);
        AddNotePresenter presenter = new AddNotePresenter(useCaseFactory);

        addNoteActivity.setPresenter(presenter);
    }
}
