package com.radroid.sampleapp.android.navigator;

import com.radroid.sampleapp.android.view.LauncherActivity;
import com.radroid.sampleapp.android.view.LoginActivity;
import com.radroid.sampleapp.android.view.NoteListActivity;

public class LauncherNavigator {

    private LauncherActivity activity;

    public LauncherNavigator(LauncherActivity activity) {
        this.activity = activity;
    }

    public void showNotesListScreen() {
        activity.startActivity(NoteListActivity.newIntent(activity));
    }

    public void showLoginScreen() {
        activity.startActivity(LoginActivity.newIntent(activity));
    }
}
