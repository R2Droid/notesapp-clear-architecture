package com.radroid.sampleapp.android.navigator;

import com.radroid.sampleapp.android.view.LoginActivity;
import com.radroid.sampleapp.android.view.NoteDetailsActivity;

public class NoteDetailsNavigator {

    private NoteDetailsActivity activity;

    public NoteDetailsNavigator(NoteDetailsActivity activity) {
        this.activity = activity;
    }

    public void showLoginScreen() {
        activity.startActivity(LoginActivity.newIntentWithNewClearTask(activity));
    }
}
