package com.radroid.sampleapp.domain;

class NoOpLogger implements CustomLogger {

    @Override
    public void d(String message, Object... params) {
        // no-op
    }

    @Override
    public void e(Throwable throwable, String message) {
        //no-op
    }
}
