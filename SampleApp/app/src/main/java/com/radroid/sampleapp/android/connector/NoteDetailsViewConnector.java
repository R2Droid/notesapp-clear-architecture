package com.radroid.sampleapp.android.connector;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.radroid.sampleapp.android.view.NoteDetailsActivity;
import com.radroid.sampleapp.data.FirebaseAuthenticationGateway;
import com.radroid.sampleapp.data.FirebaseCloudEntityGateway;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.presenter.NoteDetailsPresenter;

public class NoteDetailsViewConnector {

    public NoteDetailsViewConnector(NoteDetailsActivity noteDetailsActivity) {
        FirebaseAuthenticationGateway authGateway = new FirebaseAuthenticationGateway(FirebaseAuth.getInstance());
        FirebaseCloudEntityGateway entityGateway = new FirebaseCloudEntityGateway(FirebaseDatabase.getInstance());
        UseCaseFactory useCaseFactory = new UseCaseFactory(entityGateway, authGateway);
        String noteId = noteDetailsActivity.getIntent().getStringExtra(NoteDetailsActivity.NOTE_ID_EXTRA);
        NoteDetailsPresenter presenter = new NoteDetailsPresenter(useCaseFactory, noteId);

        noteDetailsActivity.setPresenter(presenter);
    }
}
