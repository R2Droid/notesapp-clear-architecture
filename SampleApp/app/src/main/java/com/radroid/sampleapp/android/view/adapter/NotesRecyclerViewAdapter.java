package com.radroid.sampleapp.android.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.radroid.sampleapp.R;
import com.radroid.sampleapp.presentation.presenter.NoteListPresenter;

public class NotesRecyclerViewAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    private final LayoutInflater inflater;

    private final NoteListPresenter presenter;

    public NotesRecyclerViewAdapter(Context context, NoteListPresenter presenter) {
        this.presenter = presenter;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoteViewHolder(inflater.inflate(R.layout.item_note, parent, false));
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        holder.itemView.setOnClickListener(noteItem -> presenter.onNoteItemClicked(position));

        presenter.configureNoteItemView(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getNotesListCount();
    }
}
