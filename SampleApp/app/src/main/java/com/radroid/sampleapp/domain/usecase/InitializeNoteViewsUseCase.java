package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.domain.entity.Note;

import io.reactivex.Single;

class InitializeNoteViewsUseCase implements SingleUseCase<Note> {

    private final EntityGateway entityGateway;
    private final String noteId;

    public InitializeNoteViewsUseCase(EntityGateway entityGateway, String noteId) {
        this.entityGateway = entityGateway;
        this.noteId = noteId;
    }

    @Override
    public Single<Note> execute() {
        return entityGateway.getNoteById(noteId);
    }
}
