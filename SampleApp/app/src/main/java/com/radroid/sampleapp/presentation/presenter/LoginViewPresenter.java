package com.radroid.sampleapp.presentation.presenter;

import com.radroid.sampleapp.presentation.view.LoginView;
import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;

public class LoginViewPresenter {

    private final UseCaseFactory useCaseFactory;
    private LoginView view;

    public LoginViewPresenter(UseCaseFactory useCaseFactory) {
        this.useCaseFactory = useCaseFactory;
    }

    public void onViewCreated(LoginView view) {
        this.view = view;
    }

    public void onLoginClicked(String username, String password) {
        useCaseFactory.login(username, password)
                .execute()
                .subscribe(
                        () -> view.showNoteListScreen(),
                        error -> Logger.e(error)
                );
    }
}
