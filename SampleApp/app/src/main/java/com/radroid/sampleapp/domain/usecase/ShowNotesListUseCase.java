package com.radroid.sampleapp.domain.usecase;

import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.domain.entity.Note;

import java.util.List;

import io.reactivex.Single;

class ShowNotesListUseCase implements SingleUseCase<List<Note>> {

    private final EntityGateway entityGateway;

    ShowNotesListUseCase(EntityGateway entityGateway) {
        this.entityGateway = entityGateway;
    }

    @Override
    public Single<List<Note>> execute() {
        return entityGateway.fetchNotesList();
    }
}
