package com.radroid.sampleapp.presentation.presenter;

import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.NoteViewModel;
import com.radroid.sampleapp.presentation.view.AddNoteView;

public class AddNotePresenter {

    private UseCaseFactory useCaseFactory;
    private NoteViewModel noteViewModel;
    private AddNoteView view;

    public AddNotePresenter(UseCaseFactory useCaseFactory) {
        this.useCaseFactory = useCaseFactory;
    }

    public void onSaveNoteClicked() {
        String noteTitle = noteViewModel.getTitle();

        if (noteTitle == null || noteTitle.isEmpty()) {
            view.showEmptyNoteTitleError();
        } else {

            useCaseFactory.addNote(noteViewModel)
                    .execute()
                    .subscribe(
                            () -> {
                                view.showAddNoteSuccessfulMessage();
                                view.finishWithSuccess();
                            },
                            error -> {
                                Logger.e(error);
                                view.showAddNoteFailureMessage();
                                view.finishWithFailure();
                            }
                    );
        }
    }

    public void onNoteTitleChanged(String title) {
        noteViewModel.setTitle(title);
    }

    public void onNoteContentChanged(String content) {
        noteViewModel.setContent(content);
    }

    public void onViewCreate(AddNoteView view) {
        this.view = view;
        noteViewModel = new NoteViewModel();
    }

    public void onCancelNoteClicked() {
        view.showAddNoteDiscardedMessage();
        view.finishWithFailure();
    }

    public void onLogoutClicked() {
        useCaseFactory.logout()
                .execute()
                .subscribe(() -> {
                            view.showAddNoteDiscardedMessage();
                            view.showLogoutMessage();
                            view.showLoginScreen();

                        },
                        error -> Logger.e(error.getMessage()));
    }
}

