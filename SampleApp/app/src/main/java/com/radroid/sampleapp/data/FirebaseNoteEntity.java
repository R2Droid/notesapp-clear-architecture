package com.radroid.sampleapp.data;

import com.radroid.sampleapp.domain.entity.Note;

class FirebaseNoteEntity {

    public String id;
    public String title;
    public String content;
    public long timestamp;

    public FirebaseNoteEntity() {
        // necessary empty constructor for firebase database
    }

    public FirebaseNoteEntity(Note note) {
        this.id = note.getId();
        this.title = note.getTitle();
        this.content = note.getContent();
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    Note toNote() {
        return new Note(id, title, content, timestamp);
    }
}
