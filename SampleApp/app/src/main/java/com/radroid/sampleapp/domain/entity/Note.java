package com.radroid.sampleapp.domain.entity;

public class Note {

    private final String id;
    private final String title;
    private final String content;
    private final long timestamp;

    public Note(String id, String title, String content, Long timestamp) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }
}
