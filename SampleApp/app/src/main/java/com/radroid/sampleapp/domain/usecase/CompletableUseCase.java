package com.radroid.sampleapp.domain.usecase;

import io.reactivex.Completable;

public interface CompletableUseCase {

    Completable execute();
}
