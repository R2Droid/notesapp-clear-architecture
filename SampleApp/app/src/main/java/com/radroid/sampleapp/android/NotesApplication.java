package com.radroid.sampleapp.android;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;
import com.radroid.sampleapp.domain.Logger;

public class NotesApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setLogLevel(com.google.firebase.database.Logger.Level.DEBUG);
        Logger.setLogger(new AndroidLogger());
    }
}
