package com.radroid.sampleapp.android.connector;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.radroid.sampleapp.android.view.LoginActivity;
import com.radroid.sampleapp.data.EntityGateway;
import com.radroid.sampleapp.data.FirebaseAuthenticationGateway;
import com.radroid.sampleapp.data.FirebaseCloudEntityGateway;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.presenter.LoginViewPresenter;

public class LoginViewConnector {

    public LoginViewConnector(LoginActivity loginActivity) {
        EntityGateway entityGateway = new FirebaseCloudEntityGateway(FirebaseDatabase.getInstance());
        FirebaseAuthenticationGateway authGateway  = new FirebaseAuthenticationGateway(FirebaseAuth.getInstance());
        UseCaseFactory useCaseFactory = new UseCaseFactory(entityGateway, authGateway);
        LoginViewPresenter presenter = new LoginViewPresenter(useCaseFactory);
        loginActivity.setPresenter(presenter);
    }
}
