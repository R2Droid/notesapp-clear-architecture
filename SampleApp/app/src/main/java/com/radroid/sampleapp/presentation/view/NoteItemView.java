package com.radroid.sampleapp.presentation.view;

public interface NoteItemView {

    void displayNoteTitle(String title);

    void displayNoteContent(String content);
}
