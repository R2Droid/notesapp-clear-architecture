package com.radroid.sampleapp.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.radroid.sampleapp.R;
import com.radroid.sampleapp.android.connector.AddNoteViewConnector;
import com.radroid.sampleapp.android.navigator.AddNoteNavigator;
import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.presentation.presenter.AddNotePresenter;
import com.radroid.sampleapp.presentation.view.AddNoteView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNoteActivity extends AppCompatActivity implements AddNoteView {

    @BindView(R.id.note_activity_toolbar)
    Toolbar toolbar;

    @BindView(R.id.note_activity_title_edit_text)
    EditText titleEditText;

    @BindView(R.id.note_activity_content_edit_text)
    EditText contentEditText;

    @BindString(R.string.note_empty_title_error_message)
    String emptyTitleErrorMessage;

    private AddNotePresenter presenter;
    private AddNoteNavigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        AddNoteViewConnector connector = new AddNoteViewConnector(this);
        navigator = new AddNoteNavigator(this);

        ButterKnife.bind(this);
        presenter.onViewCreate(this);
        initView();
    }

    public static Intent newIntent(NoteListActivity noteListActivity) {
        return new Intent(noteListActivity, AddNoteActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_add_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_note_menu_save:
                presenter.onSaveNoteClicked();
                break;
            case R.id.add_note_menu_logout:
                presenter.onLogoutClicked();
                break;
            case android.R.id.home:
                presenter.onCancelNoteClicked();
                break;
            default:
                Logger.e("Item id: " + item.getItemId() + " is not handled");
        }

        return super.onOptionsItemSelected(item);
    }

    public void setPresenter(AddNotePresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showAddNoteFailureMessage() {
        Toast.makeText(this, R.string.note_add_failure_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAddNoteSuccessfulMessage() {
        Toast.makeText(this, R.string.note_add_success_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishWithSuccess() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void finishWithFailure() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    public void showAddNoteDiscardedMessage() {
        Toast.makeText(this, R.string.note_add_discarded_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyNoteTitleError() {
        titleEditText.setError(emptyTitleErrorMessage);
    }

    @Override
    public void showLogoutMessage() {
        Toast.makeText(this, R.string.logout_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoginScreen() {
        navigator.showLoginScreen();
        finish();
    }

    private void initView() {
        initToolbar();
        initEditText();
    }

    private void initEditText() {
        titleEditText.addTextChangedListener(new SimpleTextWatcher() {

            @Override
            public void afterTextChanged(Editable noteTitle) {
                presenter.onNoteTitleChanged(noteTitle.toString());
            }
        });

        contentEditText.addTextChangedListener(new SimpleTextWatcher() {

            @Override
            public void afterTextChanged(Editable noteContent) {
                presenter.onNoteContentChanged(noteContent.toString());
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_cancel);
    }
}
