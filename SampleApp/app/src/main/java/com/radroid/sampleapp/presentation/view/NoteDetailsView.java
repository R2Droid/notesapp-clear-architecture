package com.radroid.sampleapp.presentation.view;

public interface NoteDetailsView {

    void showNoteInitializationError();

    void setNoteTitleView(String title);

    void setNoteContentView(String content);

    void finishWithSuccess();

    void showEditNoteErrorMessage();

    void finishWithFailure();

    void showEditNoteSuccessMessage();

    void showEmptyNoteTitleErrorMessage();

    void showEditNoteDeleteSuccessMessage();

    void showEditNoteDeleteErrorMessage();

    void showLogoutMessage();

    void showLoginScreen();

    void showNoteEditChangesDiscardedMessage();
}
