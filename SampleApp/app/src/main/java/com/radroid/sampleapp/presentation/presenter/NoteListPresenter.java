package com.radroid.sampleapp.presentation.presenter;

import com.radroid.sampleapp.domain.Logger;
import com.radroid.sampleapp.domain.entity.Note;
import com.radroid.sampleapp.domain.usecase.UseCaseFactory;
import com.radroid.sampleapp.presentation.view.NoteItemView;
import com.radroid.sampleapp.presentation.view.NoteListView;

import java.util.ArrayList;
import java.util.List;

public class NoteListPresenter {

    private final UseCaseFactory useCaseFactory;
    private NoteListView view;
    private List<Note> noteList;

    public NoteListPresenter(UseCaseFactory useCaseFactory) {
        this.useCaseFactory = useCaseFactory;
    }

    public void onViewCreated(NoteListView view) {
        this.view = view;
        refreshNoteList();
    }

    public int getNotesListCount() {
        return noteList.size();
    }

    public void onNoteItemClicked(int notePosition) {
        String noteId = noteList.get(notePosition).getId();

        view.showNoteDetailsScreen(noteId);
    }

    public void configureNoteItemView(NoteItemView holder, int position) {
        Note note = noteList.get(position);
        holder.displayNoteTitle(note.getTitle());
        holder.displayNoteContent(note.getContent());
    }

    public void onNoteListDataChanged() {
        refreshNoteList();
    }

    public void onLogoutClicked() {
        view.showLogoutMessage();
        view.showLoginScreen();
    }

    private void refreshNoteList() {
        if (noteList == null) {
            noteList = new ArrayList<>();
        }

        useCaseFactory.showNotesList()
                .execute()
                .subscribe(
                        newNotes -> {
                            noteList = newNotes;
                            view.updateNotesList(noteList);
                        },
                        error -> Logger.e(error)
                );
    }
}
